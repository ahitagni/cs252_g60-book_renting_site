from django.contrib import admin
from .models import borrower, lender, rental, listing

admin.site.register(borrower)
admin.site.register(lender)
admin.site.register(rental)
admin.site.register(listing)
# Register your models here.
