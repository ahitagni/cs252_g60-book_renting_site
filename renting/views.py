from django.shortcuts import render
from django.utils import timezone
from .models import lender, borrower, rental, listing
# Create your views here.
from django.http import HttpResponse

def index(request):
	return render(request, 'renting/homepage.html')

def lenderview(request):
	lender_names = list(lender.objects.all())
	return render(request, 'renting/lender_list.html',{'names':lender_names})

def listingview(request):
	listing_names = list(listing.objects.all())
	return render(request, 'renting/listing_list.html', {'listings':listing_names})