from django.conf.urls import url

from . import views

app_name = 'renting'
urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^lenders', views.lenderview, name='lender'),
	url(r'^listings', views.listingview, name='listing')
]