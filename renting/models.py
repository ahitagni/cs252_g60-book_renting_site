from __future__ import unicode_literals

from django.db import models

# Create your models here.



class borrower(models.Model):
    borrower_id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    email = models.CharField(max_length=50, blank=True)

    def __unicode__(self):
        return u'%s %s' % (self.first_name, self.last_name)

    class Meta:
		db_table = 'borrower'

class lender(models.Model):
    lender_id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    email = models.CharField(max_length=50, blank=True)

    def __unicode__(self):
        return u'%s %s' % (self.first_name, self.last_name)

    class Meta:
		db_table = 'lender'

class listing(models.Model):
	listing_id = models.AutoField(primary_key=True)
	book_name = models.CharField(max_length=100)
	lender = models.ForeignKey('lender')
	days = models.IntegerField()
	price = models.IntegerField()
	returned = models.BooleanField()

	def __unicode__(self):
		return u'No.%d' % self.listing_id

	class Meta:
		db_table = 'listing'

class rental(models.Model):
    rental_id = models.AutoField(primary_key=True)
    listing_id = models.ForeignKey('listing')
    rental_date = models.DateTimeField(auto_now_add=True, blank=True)
    borrower = models.ForeignKey('borrower')
    lender = models.ForeignKey('lender')

    def __unicode__(self):
        return u'No.%d' % self.rental_id

    class Meta:
		db_table = 'rental'